package com.reactlibrary;


import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.uimanager.SimpleViewManager;
import com.facebook.react.uimanager.ThemedReactContext;
import com.facebook.react.uimanager.annotations.ReactProp;
import com.microblink.detectors.DetectorResult;
import com.microblink.detectors.points.PointsDetectorResult;
import com.microblink.detectors.quad.QuadDetectorResult;
import com.microblink.hardware.orientation.Orientation;
import com.microblink.hardware.orientation.OrientationChangeListener;
import com.microblink.metadata.DetectionMetadata;
import com.microblink.metadata.Metadata;
import com.microblink.metadata.MetadataListener;
import com.microblink.metadata.MetadataSettings;
import com.microblink.recognition.InvalidLicenceKeyException;
import com.microblink.recognizers.RecognitionResults;
import com.microblink.recognizers.blinkbarcode.pdf417.Pdf417RecognizerSettings;
import com.microblink.recognizers.blinkbarcode.pdf417.Pdf417ScanResult;
import com.microblink.recognizers.settings.RecognitionSettings;
import com.microblink.recognizers.settings.RecognizerSettings;
import com.microblink.results.barcode.BarcodeDetailedData;
import com.microblink.util.CameraPermissionManager;
import com.microblink.view.CameraAspectMode;
import com.microblink.view.CameraEventsListener;
import com.microblink.view.OrientationAllowedListener;
import com.microblink.view.recognition.RecognizerView;
import com.microblink.view.recognition.ScanResultListener;
import com.microblink.view.viewfinder.PointSetView;
import com.microblink.view.viewfinder.quadview.QuadViewManager;
import com.microblink.view.viewfinder.quadview.QuadViewManagerFactory;
import com.microblink.view.viewfinder.quadview.QuadViewPreset;

import com.facebook.react.modules.core.DeviceEventManagerModule;

import javax.annotation.Nonnull;


/**
 * Created by wsche on 2017/11/23.
 */

public class MicroBlinkManager extends SimpleViewManager<FrameLayout>
        implements LifecycleEventListener, ScanResultListener, CameraEventsListener, MetadataListener, OrientationChangeListener {

    public static final String REACT_CLASS = "RNMicroBlinkScanner";
    public static final int RECOGNIZERVIEW_ID = 123456789;
    /**
     * This is built-in helper for built-in view that draws detection location
     */
    private QuadViewManager mQvManager = null;
    private RecognitionSettings mRecognitionSettings = new RecognitionSettings();
    private boolean hasBeenCreated;
    /**
     * Container for the Recognizer view so that we can add extra overlays
     */
    private FrameLayout mFrameLayout;
    /**
     * RecognizerView is the builtin view that controls camera and recognition
     */
    private RecognizerView mRecognizerView;
    /**
     * CameraPermissionManager is provided helper class that can be used to obtain the permission to use camera.
     * It is used on Android 6.0 (API level 23) or newer.
     */
    private CameraPermissionManager mCameraPermissionManager;
    /**
     * This is a builtin point set view that can visualize points of interest, such as those of QR code
     */
    private PointSetView mPointSetView;
    private ThemedReactContext mContext;

    public MicroBlinkManager() {
    }

    public CameraPermissionManager getCameraPermissionManager() {
        return mCameraPermissionManager;
    }

    public RecognizerView getRecognizerView() {
        return mRecognizerView;
    }

    @Override
    public String getName() {
        return REACT_CLASS;
    }

    @Override
    public FrameLayout createViewInstance(ThemedReactContext context) {
        mContext = context;
        ViewGroup.LayoutParams layoutParams = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        mFrameLayout = new FrameLayout(context);
        mFrameLayout.setLayoutParams(layoutParams);

        mRecognizerView = new RecognizerView(context);
        mRecognizerView.setId(RECOGNIZERVIEW_ID);
        mRecognizerView.setLayoutParams(layoutParams);
        mFrameLayout.addView(mRecognizerView);
        mContext.addLifecycleEventListener(this);

        return mFrameLayout;
    }

    @Override
    public void onAutofocusFailed() {
        // this method is called when camera autofocus fails
    }

    @Override
    public void onAutofocusStarted(Rect[] rects) {
        // rects array define array of rectangles in view's coordinate system where
        // focus metering is taking place
    }

    @Override
    public void onAutofocusStopped(Rect[] rects) {
        // remove focusing animation
        // rects array define array of rectangles in view's coordinate system where
        // focus metering is taking place
    }

    @Override
    public void onCameraPreviewStarted() {
        // if device supports torch, make torch button visible and setup it
        // isCameraTorchSupported returns true if device supports controlling the torch and
        // camera preview is active
        setScreenOrientation(Orientation.ORIENTATION_PORTRAIT);
    }

    @Override
    public void onCameraPreviewStopped() {
        // this method is called just after camera preview has stopped
    }

    @Override
    public void onError(Throwable ex) {
        // This method will be called when opening of camera resulted in exception or
        // recognition process encountered an error.
        // The error details will be given in ex parameter.
        com.microblink.util.Log.e(this, ex, "Error");
    }

    @Override
    public void onHostResume() {
        // Activity `onResume`
        // all activity lifecycle events must be passed on to RecognizerView
        if (mRecognizerView != null) {
            mRecognizerView.resume();
        }
    }

    @Override
    public void onHostPause() {
        // Activity `onPause`
        // all activity lifecycle events must be passed on to RecognizerView
        if (mRecognizerView != null) {
            mRecognizerView.pause();
        }
    }

    @Override
    public void onHostDestroy() {
        // Activity `onDestroy`
        // all activity lifecycle events must be passed on to RecognizerView
        if (mRecognizerView != null) {
//            mRecognizerView.destroy();
        }
    }

    @Override
    public void onMetadataAvailable(Metadata metadata) {
        // This method will be called when metadata becomes available during recognition process.
        // Here, for every metadata type that is allowed through metadata settings,
        // desired actions can be performed.

        // detection metadata contains detection locations
        if (metadata instanceof DetectionMetadata) {
            // detection location is written inside DetectorResult
            DetectorResult detectorResult = ((DetectionMetadata) metadata).getDetectionResult();
            // DetectorResult can be null - this means that detection has failed
            if (detectorResult == null) {
                // this metadata object indicates that during recognition process nothing was detected.
                if (mPointSetView != null) {
                    // clear points
                    mPointSetView.setPointsDetectionResult(null);
                }
                if (mQvManager != null) {
                    // begin quadrilateral animation to its default position
                    // (internally displays FAIL status)
                    mQvManager.animateQuadToDefaultPosition();
                }
                // when points of interested have been detected (e.g. QR code), this will be returned as PointsDetectorResult
            } else if (detectorResult instanceof PointsDetectorResult) {
                // show the points of interest inside points view
                mPointSetView.setPointsDetectionResult((PointsDetectorResult) detectorResult);
                // when object represented by quadrilateral is detected, this will be returned as QuadDetectorResult
            } else if (detectorResult instanceof QuadDetectorResult) {
                // begin quadrilateral animation to detected quadrilateral
                mQvManager.animateQuadToDetectionPosition((QuadDetectorResult) detectorResult);
                if (mPointSetView != null) {
                    // clear points
                    mPointSetView.setPointsDetectionResult(null);
                }
            }
        }
    }

    private void setScreenOrientation(Orientation orientation){
        // Wrap the new screen Orientation in a Configuration object
         Configuration newConfig = new Configuration();
         newConfig.orientation = orientation.intValue();

        // Update the MainActivity configuration
         mContext.getCurrentActivity().onConfigurationChanged(newConfig);

         if (mRecognizerView == null) {
             return;
         }
         // Update recognizer view configuration
         mRecognizerView.changeConfiguration(newConfig);
    }

    @Override
     public void onOrientationChange(Orientation orientation) {
         setScreenOrientation(orientation);
     }

    /**
     * this activity will perform 5 scans of barcode and then return the last
     * scanned one
     */
    @Override
    public void onScanningDone(RecognitionResults results) {
        if (results != null && results.getRecognitionResults() != null && results.getRecognitionResults().length > 0) {

            Pdf417ScanResult result = (Pdf417ScanResult) results.getRecognitionResults()[0];
            // getStringData getter will return the string version of barcode contents
            String barcodeData = result.getStringData();
            // isUncertain getter will tell you if scanned barcode contains some uncertainties
            boolean uncertainData = result.isUncertain();
            // getRawData getter will return the raw data information object of barcode contents
            BarcodeDetailedData rawData = result.getRawData();
            // BarcodeDetailedData contains information about barcode's binary layout, if you
            // are only interested in raw bytes, you can obtain them with getAllData getter
            byte[] rawDataBuffer = rawData.getAllData();
            String base64EncodedString = Base64.encodeToString(rawDataBuffer, Base64.NO_WRAP);

            WritableMap eventData = Arguments.createMap();

            eventData.putString("type", "PDF417");
            eventData.putString("barcode", base64EncodedString);

            mContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                    .emit("onBarcodeDetected", eventData);

            mRecognizerView.pauseScanning();
            mRecognizerView.pause();
            mRecognizerView.stop();
            mRecognizerView.destroy();
            mRecognizerView = null;
        }
    }

    @ReactProp(name = "licence")
    public void onCreate(FrameLayout view, @Nonnull String key) {
        // setup scanner parameters
        try {
            mRecognizerView.setLicenseKey(key);
        } catch (InvalidLicenceKeyException e) {
            e.printStackTrace();
            Toast.makeText(mContext, "Invalid licence key", Toast.LENGTH_SHORT).show();
        }
        // add settings objects to recognizer settings array
        // Pdf417Recognizer and ZXingRecognizer will be used in the recognition process
        mRecognitionSettings.setRecognizerSettingsArray(
                new RecognizerSettings[]{new Pdf417RecognizerSettings()});

        mRecognizerView.setRecognitionSettings(mRecognitionSettings);

        // add listeners
        mRecognizerView.setScanResultListener(this);
        mRecognizerView.setCameraEventsListener(this);

        // orientation allowed listener is asked if orientation is allowed when device orientation
        // changes - if orientation is allowed, rotatable views will be rotated to that orientation
        mRecognizerView.setOrientationAllowedListener(new OrientationAllowedListener() {
            @Override
            public boolean isOrientationAllowed(Orientation orientation) {
                // allow all orientations
                return true;
            }
        });

        // define which metadata will be available in MetadataListener (onMetadataAvailable method)
        MetadataSettings metadataSettings = new MetadataSettings();
        // detection metadata should be available in MetadataListener
        // detection metadata are all metadata objects from com.microblink.metadata.detection package
        metadataSettings.setDetectionMetadataAllowed(true);
        // set metadata listener and defined metadata settings
        // metadata listener will obtain selected metadata
        mRecognizerView.setMetadataListener(this, metadataSettings);

        mRecognizerView.setInitialOrientation(Orientation.ORIENTATION_PORTRAIT);
        Orientation orient = mRecognizerView.getInitialOrientation();

        System.out.println(orient);
        // set camera aspect mode to FILL - this will use the entire surface
        // for camera preview, instead of letterboxing it
        mRecognizerView.setAspectMode(CameraAspectMode.ASPECT_FILL);

        // animate rotatable views on top of scanner view
        mRecognizerView.setAnimateRotation(true);

        // instantiate the camera permission manager
        mCameraPermissionManager = new CameraPermissionManager(mContext.getCurrentActivity());
        // get the built in layout that should be displayed when camera permission is not given
        View v = mCameraPermissionManager.getAskPermissionOverlay();
        if (v != null) {
            // add it to the current layout that contains the recognizer view

            mFrameLayout.addView(v);
        }

        // create scanner (make sure scan settings and listeners were set prior calling create)
        mRecognizerView.create();

        // after scanner is created, you can add your views to it

        // initialize QuadViewManager
        // Use provided factory method from QuadViewManagerFactory that can instantiate the
        // QuadViewManager based on several presets defined in QuadViewPreset enum. Details about
        // each of them can be found in javadoc. This method automatically adds the QuadView as a
        // child of RecognizerView.
        // Here we use preset which sets up quad view in the same style as used in built-in PDF417 ScanActivity.
        mQvManager = QuadViewManagerFactory.createQuadViewFromPreset(mRecognizerView, QuadViewPreset.DEFAULT_CORNERS_FROM_PDF417_SCAN_ACTIVITY);

        // create PointSetView
        mPointSetView = new PointSetView(mContext, null, mRecognizerView.getHostScreenOrientation());

        // add point set view to scanner view as fixed (non-rotatable) view
        mRecognizerView.addChildView(mPointSetView, true);

        mRecognizerView.start();
//        mRecognizerView.resume();
    }

    @TargetApi(23)
    @Override
    public void onCameraPermissionDenied() {
        // this method is called on Android 6.0 and newer if camera permission was not given
        // by user

        // ask user to give a camera permission. Provided manager asks for
        // permission only if it has not been already granted.
        // on API level < 23, this method does nothing
        mCameraPermissionManager.askForCameraPermission();
    }

}
