
package com.reactlibrary;

import android.annotation.TargetApi;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

public class RNMicroBlinkScannerModule extends ReactContextBaseJavaModule {

  private final ReactApplicationContext reactContext;
  private MicroBlinkManager mMicroBlinkManager;

  public RNMicroBlinkScannerModule(ReactApplicationContext reactContext, MicroBlinkManager microBlinkManager) {
    super(reactContext);
    this.reactContext = reactContext;
    this.mMicroBlinkManager = microBlinkManager;
  }

  @Override
  public String getName() {
    return "RNMicroBlinkScannerModule";
  }

  @ReactMethod
  public void onStart() {
    // all activity lifecycle events must be passed on to RecognizerView
    if (mMicroBlinkManager.getRecognizerView() != null) {
      mMicroBlinkManager.getRecognizerView().start();
    }
  }

  @ReactMethod
  public void onStop() {
    // all activity lifecycle events must be passed on to RecognizerView
    if (mMicroBlinkManager.getRecognizerView() != null) {
      mMicroBlinkManager.getRecognizerView().stop();
    }
  }

  @TargetApi(23)
  @ReactMethod
  public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
    // on API level 23, we need to pass request permission result to camera permission manager
    mMicroBlinkManager.getCameraPermissionManager().onRequestPermissionsResult(requestCode, permissions, grantResults);
  }
}