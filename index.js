import React, { Component } from 'react'
import {
    Platform,
    requireNativeComponent,
    DeviceEventEmitter
} from 'react-native'
import PropTypes from 'prop-types'

class MicroBlinkScanner extends Component {
    static propTypes = {
        licence: PropTypes.string.isRequired,
        onBarcodeDetected: PropTypes.func
    }
    
    _onBarcodeDetected = (event) => {
        if (!this.props.onBarcodeDetected) {
            return;
        }
        let evt = Platform.OS === 'android' ? event : event.nativeEvent
        this.props.onBarcodeDetected(evt)
    }

    componentWillMount() {
        // If scanning were successful, onBarcodeDetected will be triggered:
        DeviceEventEmitter.addListener('onBarcodeDetected', this._onBarcodeDetected)
    }

    componentWillUnmount() {
        if (Platform.OS === 'android') {
            DeviceEventEmitter.removeListener('onBarcodeDetected', this._onBarcodeDetected)
        }
    }


    render() {
        const nativeProps = {
            ...this.props,
            onBarcodeDetected: this._onBarcodeDetected,
        }
        return <RNMicroBlinkScanner {...nativeProps} />
    }
}

// Get the native bridge for the scanner
let RNMicroBlinkScanner

RNMicroBlinkScanner = requireNativeComponent('RNMicroBlinkScanner', RNMicroBlinkScanner, { nativeOnly: { onBarcodeDetected: true } })

// Export
module.exports = MicroBlinkScanner