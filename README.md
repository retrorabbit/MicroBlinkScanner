
# react-native-micro-blink-scanner

## Getting started

`$ npm install react-native-micro-blink-scanner --save`

### Mostly automatic installation

`$ react-native link react-native-micro-blink-scanner`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-micro-blink-scanner` and add `RNMicroBlinkScanner.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNMicroBlinkScanner.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<
e
#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.reactlibrary.RNMicroBlinkScannerPackage;` to the imports at the top of the file
  - Add `new RNMicroBlinkScannerPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-micro-blink-scanner'
  	project(':react-native-micro-blink-scanner').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-micro-blink-scanner/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-micro-blink-scanner')
  	```

## Usage
```javascript
import RNMicroBlinkScanner from 'react-native-micro-blink-scanner';

// TODO: What to do with the module?
RNMicroBlinkScanner;
```
####Android
#####The following methods need to be executed on the RNMicroBlinkScanner instance if the view is rendered

1. These need to be called in response to the equivalently named event in the Android Activity
    ```
      onStart()  
      onStop() 
      onConfigurationChanged(Configuration newConfig)  
    ```
2. If CAMERA permissions haven't been allowed before the RNMicroBlinkScanner instance is in view 
request the permissions and call the equivalent methods on the RNMicroBlinkScanner instance in the Activity
    ```
    onCameraPermissionDenied() 
    onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) 
    ```
3. Before the view is disposed call `onStop` on the RNMicroBlinkScanner instance
